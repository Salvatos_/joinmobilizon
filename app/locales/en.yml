meta:
  title: '@:home.title ! #JoinMobilizon'
  description: '@:home.intro.title'
menu:
  faq: F.A.Q.
  help: Help
  docs: Documentation
  code: Source code
  instances: Instances
  hall-of-fame: Hall of fame
  news: News
link:
  forumPT: https://framacolibri.org/c/qualite/mobilizon
home:
  title: Let’s retake control of our events
  subtitle: Together, we go further
  intro:
    tags:
    - Gather
    - Organise
    - Mobilise
    title: A user-friendly, emancipatory and ethical tool for gathering, organising,
      and mobilising.
    support: Support us
  intro2:
    title: A free and federated tool to get our events off Facebook!
  what:
    text1: Mobilizon is a tool designed to create platforms for <b>managing communities
      and events</b>. Its purpose is to help as many people as possible emancipate
      themselves from Facebook groups and events, Meetup, etc.
    text2: Since the Mobilizon software is distributed under a Free license, anyone can host
      a Mobilizon server, called an <b>instance</b>. These instances can form a federation,
      so that a person with an account on <i>ExampleMeet</i> can register for an event
      posted on <i>SpecimenEvent</i>.
  project:
    p:
    - From a birthday party with friends and family to a march for climate change, right now,
      our gatherings are trapped inside the tech giants’ platforms. How can we organise,
      how can we click “Attend,” without providing private data to Facebook or locking
      ourselves up inside MeetUp?
    - 'Framasoft wants to create <b>Mobilizon</b>: free/libre software that
      will allow communities to create their own spaces to publish events, in order
      to better emancipate themselves from tech giants. We want to develop a <b>digital
      common</b>, that everyone can make their own, which <b>respects privacy and activism</b>
      by design.'
    - We asked professional designers to help us develop our vision for Mobilizon. We took
      time to study the digital habits of activists in order to understand the features they
      need to gather, organise, and mobilise.
    - We want to make Mobilizon a user-friendly, emancipatory and ethical tool. Whether
      you want to create an event in one of the Mobilizon instances or to set up
      your own Mobilizon site… the features of this tool will depend on
      the means you give us to develop it.
    legend:
    - Layout of an event page – G. Dorne and M.-C. Paccard
    - Layout of a group page – G. Dorne and M.-C. Paccard
  steps:
    title: A beta version in the fall of 2019
    intro: 'This fundraising will determine the resources we can count on to work on the
      software, in order to release a beta version in the fall of 2019</b>. We have
      60 days to let people know about this fundraising and find out what you want to see
      in Mobilizon. Here are the (cumulative) milestones that your money can help fund:'
    minimaliste:
      title: Milestone 1
      subtitle: Free & Basic
      ul:
      - Event-publishing tool
      - UX studies
      - Graphic design
      - Free & documented code
      p:
      - This amount will cover our prototype expenses and help us complete it.
      - It will compensate us for the money we fronted to design and promote the software,
        to develop its basic functionality and to pay the UX and UI designers who
        facilitated its conception.
      - The code will be returned to the community so that anyone can take over and
        maintain it.
    federe:
      title: Milestone 2
      subtitle: Emancipating & Federated
      ul:
      - Federation and links
      - Administration tools
      - Test/demo instance
      - ActivityPub integration
      p:
      - 'This will give us the means to implement a protocol that is essential to Mobilizon’s success:
        the ActivityPub federation. Thanks to the federation, any community will be
        able not only to install Mobilizon on its own servers, but also to connect
        and synch with others.'
      - This will help the number of entry points to grow and multiply, decentralise
        the data, and  connect to a fediverse that already includes Mastodon (a federated
        alternative to Twitter) and PeerTube (a federated alternative to YouTube).
      - We will also open a demo instance of this beta version, so that everyone can
        go and see for themselves what the software will feel like.
    ideal:
      title: Milestone 3
      subtitle: Ideal & Friendly
      ul:
      - Group management
      - Messaging
      - Multiple identities
      - Management of external tools
      p:
      - This will give us the means to make the Mobilizon of our dreams come true!
      - Users will be able to create and manage groups in Mobilizon, with messaging
        tools to facilitate exchange between members. Mobilizon will also be able to
        display, organise and manage the external tools that you already use to create
        documents together, set the next meeting date, etc.
      - Lastly, each account holder will be able to create several identities,
        in order, for example, to display a different identity for sporting events,
        family reunions and activism marches.
    version-1:
      title: and beyond…?
      subtitle: Durable & Resilient
      ul:
      - Funding beyond V1
      - Advanced mapping
      - Improved interface
      - Mobile application
      p:
      - 'We did the math: with €50,000, we will have the means to make
        the best software we can. If we receive more money, we won’t do more,
        or faster, but we can go further.'
      - In the fall of 2019, we will release a beta version. From the feedback and
        comments we will then receive, we will work on a complete first release, a
        V1 which we expect to be ready in the first half of 2020.
      - But there’s a lot of work to do until we get there! For example,
        we would like to set up a mapping server to facilitate the localisation
        of events, create a Mobilizon app for smartphones, and much more…
    understand: Learn more about those milestones
  allocation:
    title: Where is your money going?
    if: If we get
    intro:
    - We are running this fundraiser on a self-hosted website in order to reduce bank
      and crowdfunding fees. Our goal is to promote a new model of funding for digital commons,
      based on <a href="https://joinpeertube.org/en/news">our experience with PeerTube</a>.
    - Sensitive to the issues caused by the centralisation of event management on Facebook, we
      used our own funds to design and prototype an alternative tool, Mobilizon. Now, we
      want to know what you think of this project. The more you give, the more
      we can dedicate ourselves to the development and maintenance of Mobilizon.
    expenses:
    - Payment fees
    - Design and prototyping
    - Development
    - Project coordination
    - Communication
    steps:
    - We’ve already paid for the prototyping and design of Mobilizon, by hiring designers and
      writing the baseline code. Reaching this first milestone will reimburse the sums we paid
      upfront and allow us to finalise the code so we can deliver it to the community.
    - Most of the sums collected at the 2<sup>nd</sup> milestone will fund the integration
      of the ActivityPub federation protocol, as well as the development of administration
      tools for future hosters of instances.
    - 'The 3<sup>rd</sup> milestone will give us the means to design and develop features
      that Facebook and the like have no interest in giving their users: group self-management,
      messaging, multiple identities, and external tool management.'
  donation:
    title: Mobilizon is supported by Framasoft, a French not-for-profit.
    texts:
    - '@:data.html.soft (that’s us!) is a non-profit association created in 2004, which
      now focuses on <b>popular education</b> on the stakes of our digital world.
      Our small organisation (fewer than 40 members, fewer than 10 employees) is known
      to have carried out the project <a href="@:data.link.dio">Degooglise the Internet</a>,
      which proposes 34 ethical and alternative online tools.'
    - Since 2017, we have been developing <a href="https://joinpeertube.org/">PeerTube</a>,
      a free and federated alternative to YouTube. Thanks to your donations from a
      previous crowdfunding campaign, PeerTube now enjoys growing success. Bolstered by
      this experience, we imagined the Mobilizon project.
    - Recognised for its service of public interest, our organisation is more than 90% funded
      through <b>donations</b>, mainly from our French supporters. Today, we need
      your generosity to help us make Mobilizon a reality throughout the world.
  team:
    title: About the team
    more: More about @:data.color.soft
    guests:
      title: Guest Designers
      intro: We asked two designers to work with us on this project, so that
        Mobilizon would, right from its conception, fit the needs and uses of the
        people who are going to use it.
    members:
      mcgp:
        desc: “Independent designer, UX researcher who seeks to thoroughly understand
          usability and design user-friendly, ethical and inclusive tools.”
      dorne:
        desc: “Independent designer and founder of Design & Human.”
  funding:
    goal: raised on the way to
    donators: contributors
    days: days
    hours: hours
    deadline: to go
    step-on: (milestone {step} of {max})
    share: Share and follow
    news: Stay up-to-date with Mobilizon
    news-alt: Newsletter
    who: Project leader
    asso: The not-for-profit @:data.html.soft
    where: Lyon, France
    when-from: Running from
    when-to: to
    more: more info
    anonymous: Anonymous
    rewards:
      title: My contribution
      d1:
        title: €1 and more
        subtitle: <small>equivalent to </small> a hug to the team!
        p: 'You show us your trust and support, and you contribute to the birth
          of Mobilizon: THANK YOU!'
      d25:
        title: €25 and more
        subtitle: <small>equivalent to</small> 15 minutes of conversation
        p: Meetings, phone calls and interviews… Sometimes, creating user-friendly
          software requires more spit than sweat!
      d42:
        title: €42 and more
        subtitle: <small>equivalent to</small> one hour of programming
        p: Accounting for payroll expenses, offices, equipment… this is the price of
          code written in humane conditions.
      d88:
        title: €88 and more
        subtitle: <small>equivalent to</small> a brainstorm
        p: Gathering around a whiteboard… that’s how the most beautiful ideas
          are born!
      d130:
        title: €130 and more
        subtitle: <small>equivalent to</small> one UX interview
        p: If you want to design software made for others, you need to ask
          them about their needs.
      donators: Contributors
      free: Choose your own amount
    sponsors:
      title: Sponsors’ corner
      intro: For organisations that want to display their support to the project, for
        the entire lifespan of V1, subject to acceptance by the association.
      d1000:
        title: €1000 and more
        p: Your logo will be highlighted in the joinmobilizon.org hall of fame.
      d2500:
        title: €2500 and more
        p: Your logo will also be shown, by default, in the “About” page of the software.
  quote:
    text: We won’t change the world from Facebook. The tool we dream of, surveillance
      capitalism corporations won’t develop it, as they couldn’t profit from it. <br>
      This is an opportunity to build something better, by taking another approach.
    blog:
      text: Read Framasoft’s statement of intent on the Framablog
      link: https://framablog.org/2019/05/14/mobilizon-lets-finance-a-software-to-free-our-events-from-facebook/
  how-it-works:
    how:
      text:
        convivial:
          icon: group
          title: User-friendly and convenient
          text: Mobilizon doesn’t try to lock you inside its platform to manage your community
            nor to direct your ways.<br> On the countrary, its goal is to help you
            integrate the collaborative tools of your choice, and to let you be free
            to organise your community and gatherings your own way.
        emancipation:
          icon: address-card
          title: Emancipating and respectful
          text: Mobilizon gives you the ability to engage without revealing yourself,
            to organise without exposing yourself. For instance, with just one account,
            you will get several identities, used as social masks.
        ethic:
          icon: smile-o
          title: Ethical and decentralised
          text: The Free licence of Mobilizon software is a guarantee of its transparency,
            its contributive aspect and the openness of its governance. Having several
            Mobilizon instances will let you choose where you want to create your
            account, so you can decide to whom you will entrust your data.
  timeline:
    title: And next… our roadmap
    image-alt: Illustration of Mobilizon in Contributopia
    events:
      init:
        date: October 2018
        text: Announcement of Framasoft’s intentions for Mobilizon
      study:
        date: End 2018 - Beginning 2019
        text: UX studies, usage-centred design and prototyping
      crowdfunding:
        date: May 2019
        text: <strong>Crowdfunding to determine the means we have to develop Mobilizon</strong>
      beta:
        date: Fall 2019
        text: Release of the beta version based on the funding secured
      rc:
        date: End of 2019
        text: Improvements based on the first uses in beta
      v1:
        date: 1st half of 2020
        text: Release of V1…?
  contribute:
    title: We want to co-create Mobilizon with you to make it the tool we all
      dream of
    text: Want to get involved in this project, but you don’t know where to begin?
    ideas:
      support:
        title: Support Framasoft
        text: Support Framasoft financially, who has invested in this project
        link: https://soutenir.framasoft.org/
        icon: euro
      talk_about:
        title: Talk about Mobilizon to your loved ones
        text: If you know people who may be interested in the Mobilizon project, tell
          them about it!
        icon: share
      feedback:
        title: Share your feedback
        text: Are you happy with Mobilizon? Tell us about it!
        link: https://framacolibri.org/c/mobilizon
        icon: how to do it
      discuss:
        title: Join the discussion
        text: Help us shape the future of Mobilizon
        link: https://framacolibri.org/c/mobilizon
        icon: how to do it
      translate:
        title: Translate Mobilizon
        text: Do you speak a different language? Help translate Mobilizon’s interface.
        icon: language
      ask_functionnality:
        title: Request a feature
        text: Are you missing something? Describe the feature you’re dreaming of and engage
          in the conversation
        link: https://framacolibri.org/c/mobilizon/feature
        icon: question
      install:
        title: Install Mobilizon
        text: Take your place in the federation and let’s exchange!
        link: https://framagit.org/framasoft/mobilizon/wikis/install
        icon: server
    utopia: Let’s give people the tools to make the world of their dreams a reality.
form:
  step1:
    title: Contribute
    oneshot: One-time donation
    oneshot_ex: 'e.g.: 42'
    other: Amount
    newsletter: I want to be informed of Mobilizon’s progress
    anonymous: I want my donation to remain anonymous
    defisc: I would like to receive a tax receipt
  step2:
    intro: Please complete this information so we can prepare your receipt
    private: Privacy
    type: I represent
    part: an individual
    corp: a company
    corp_tip: Company, association, community…
    society: Legal entity
    society_ex: e.g. Free Software Foundation
    society_email_ex: e.g. contact@fsf.org
    nickname: Nickname
    nickname_ex: e.g. rms
    lastname: Last name
    lastname_ex: e.g. Stallman
    firstname: First name
    firstname_ex: e.g. Richard
    email: E-mail
    email_ex: e.g. r.stallman@outlook.com
    address1: Address
    address1_ex: e.g. 12, Freedom Street
    address2: Address line 2
    address2_ex: e.g. Building VI
    zip: Postal/Zip code
    zip_ex: e.g. 69007
    city: City
    city_ex: e.g. Lyon
    country: Country
    error_empty: This field must be filled in.
    error_email: You must enter a valid e-mail address.
  step3:
    title: Payment method
    defisc_text: In France, thanks to the {percent} tax deduction, <b>your donation
      of {amount}</b> will cost you {defisc}.
    cb: Credit card
    pp: Paypal
    i_give: Give
    now: now
contact:
  title: Contact us!
  source: Source
  forum:
    link: https://framacolibri.org/c/mobilizon/
footer:
  credits: '© <a href="https://framasoft.org/en/">Framasoft</a> 2018 <a href="https://framagit.org/framasoft/joinmobilizon/joinmobilizon">AGPL-3.0</a>
    | '
  photo: ' <a href="https://pixabay.com/fr/action-collaborer-collaboration-2277292/">Picture</a>
    by rawpixel on Pixabay | '
  picture: ' Illustrations by <a href="https://twitter.com/ninalimpi">Katerina Limpitsouni</a>
    for <a href="https://undraw.co/">Undraw</a> and <a href="https://davidrevoy.com">David
    Revoy</a> for <a href="https://contributopia.org/">Contributopia</a>.'
merci:
  title: Thank you very much for your donation to the Mobilizon project!
  continue: Thanks to you, Framasoft’s teams will be able to continue and
    expand the work already started on this software.
  refresh: 'If your donation is not anonymous, you should see your name appear in the Hall of Fame page within the next 10 minutes (please refresh to check).'
  success: 'The success of this crowdfunding depends only on you: we have until July
    10 to find out, together, how much this project resonates with people and what resources
    can be allocated to develop it.'
  comment: Feel free to share your gesture on your favourite social networks
    and to talk about Mobilizon around you…
  share: Join me and support Mobilizon, a free and federated tool to get our events
    off Facebook!
  shareOn: Share on
  shareTitle: Let’s support Mobilizon
hof:
  title: Hall of Fame
  sponsors: Sponsors
  donators: Donators
  dev: Contributors
  contrib: Contribute to the code
faq:
  title: Learn more about Mobilizon…
  clic: (click each question to reveal the answer)
  prez:
    title: Presentation of Mobilizon
    what:
      q: What is Mobilizon?
      a:
        p:
        - It’s software that, once installed on a server, will create a website where
          people can create events, similar to Facebook events or MeetUp.
        - Installing Mobilizon will allow communities to free themselves from the
          services of tech giants by creating their own event platform. This installation
          (called “instance“) can easily interconnect with others like it, thanks to
          <a href="https://en.wikipedia.org/wiki/ActivityPub">a decentralised federation protocol</a>.
        - Although it can be used to organise a simple birthday party or a karate competition,
          Mobilizon will be developed to meet the needs of activists, who require specific
          tools to organise themselves in an autonomous and sustainable way.
    facebook:
      q: Mobilizon will not replace Facebook
      a:
        p:
        - By design, Mobilizon is not meant to be a “Facebook killer.”
        - We can see the danger of publishing an activism rally on Facebook, a monopolistic
          platform that <a href="https://dayssincelastfacebookscandal.com/">multiplies
          scandals</a> about privacy and the manipulation of public opinion. But compared
          to this tech giant, our means are modest.
        - 'Our ambition goes accordingly: let’s start with a tool that does little
          but does it well, and that will make a solid foundation for building more.
          Let’s focus first on the specific needs of a particular audience (activists),
          which won’t prevent Mobilizon from being useful in other cases. Then,
          in the long run, we can adapt it further to the needs of other audiences.'
        - Making Mobilizon is therefore not a sprint, where we promise everything at once
          to everyone. It’s more of a cross-country race, where the first step
          is to develop a tool that empowers anyone to create events without a fuss.
    features:
      q: Will Mobilizon allow me to…?
      a:
        p:
        - 'Make recurring events? Share my events on Twitter? Send private messages?
          Our ideas and wishes for Mobilizon are as numerous as our supporters…
          There is but one answer:'
        - It depends! :)
        - The list of features for V1 will depend on the amount of money raised.
        - And what happens after V1 will depend on the Mobilizon community.
        - Though this campaign's success is more dependent on you than on us, we can
          promise that we will keep you informed of Mobilizon’s progress at each new
          stage, on <a href="@:data.baseurl@:lang/news">the news page here</a>
          and in our dedicated newsletter.
    advantages:
      q: What are the 3 benefits of using Mobilizon?
      a:
        p:
        - '<b>Free</b>: Mobilizon’s licence guarantees respect of the fundamental
          freedoms of the people who will use it. Since <a href="https://framagit.org/framasoft/mobilizon/">its source
          code is public</a>, anyone can audit it, which guarantees its
          transparency. If the direction given by the development team does not suit
          you, you have the legal right to create your own version of the software, with
          your own governance choices.'
        - '<b>Ethical</b>: Mobilizon is developed on a non-profit basis, so there
          will be no profiling nor attention-grabbing mechanism. On the contrary,
          it is designed to give maximum power to the people who use it.'
        - '<b>Human</b>: Mobilizon is not developed by a secretive start-up,
          but by a group of friends who strive to <a href="https://framasoft.org">change
          the world, one byte at a time</a>. So while we do work slower, we remain
          attentive and in touch with our users. Moreover, Mobilizon was designed by
          asking activists how they use digital tools.'
    federated:
      q: Why is a federated tool better?
      a:
        p:
        - 'Let’s imagine that my university creates its instance <em>MobilizeCollege</em>
          on the one hand, and my climate defence movement creates its instance <em>EcoMobilized</em>
          on the other hand: do I need to create an account on each site, just to
          keep up with the events?'
        - 'No: in our opinion, this would be a major obstacle to use. That’s why we
          want Mobilizon to be federated: each instance (each event publication website)
          powered by Mobilizon will be able to choose to exchange with other
          instances, to display more events than the ones it hosts, and to promote interaction.'
        - The federation protocol, which uses the most widespread standard (<a href="https://en.wikipedia.org/wiki/ActivityPub">ActivityPub</a>),
          will also allow, in the long run, to build bridges with <a href="https://joinmastodon.org">Mastodon</a>
          (the free and federated alternative to Twitter), <a href="https://joinpeertube.org">PeerTube</a>
          (the free and federated alternative to YouTube), and many other alternative tools.
    install:
      q: How do I install Mobilizon on a server?
      a:
        p:
        - 'For now, you can’t: the code is still under development
          and there are no installation guidelines yet.'
        - You will need to wait for the release of the beta version (fall 2019)
          and version 1 (first half of 2020) to see installation facilitated
          by complete documentation, and even packaging.
        - However, if you want to keep an eye on our work in progress, <a href="https://framagit.org/framasoft/mobilizon">the
          source code is here</a>.
    develop:
      q: How can I contribute code to Mobilizon?
      a:
        p:
        - First of all, you will need knowledge of Git and Elixir. If you are not familiar
          with them, the project is not in a position to receive your contributions
          for the time being.
        - If you do, simply go to <a target="_blank" rel="noopener noreferrer" href="https://framagit.org/framasoft/mobilizon/">the
          software repository</a> and send in an issue, or fork the code to start submitting
          your own contributions.
        - Of course, it’s always better to come and talk with our developers beforehand,
          by using <a target="_blank" rel="noopener noreferrer" href="https://riot.im/app/#/room/#Mobilizon:matrix.org">our
          Matrix</a> room.
    contribute:
      q: How can I contribute to Mobilizon if I’m not a programmer?
      a:
        p:
        - The easiest way is to come and talk to us, in <a target="_blank" rel="noopener
          noreferrer" href="https://framacolibri.org/c/mobilizon">the Mobilizon
          area</a> of our contributions forum.
        - 'Remember that we are not a multinational tech giant, nor even a large start-up,
          but a mere not-for profit with fewer than 40 members (which also manages other
          projects in parallel): don’t be offended if we need time to get back to you!'
        - You probably have some great ideas to add to the project, and we thank you
          for taking the time to share them. However, we know that we can deliver
          what we promised for the beta version and V1, but we also know that
          we cannot add to our already ambitious schedule.
        - Thus, you should expect that your wishes and proposals will not be implemented
          until after version 1, which we (Framasoft) hope to be able to bring to completion
          (but that will depend on the success of this crowdfunding campaign!).
    cost:
      q: How much will it cost me to use Mobilizon?
      a:
        p:
        - The aim of crowdfunding the creation of this software is that a large number
          of organisations can then make it available for free, as is done for most <a href="https://joinmastodon.org">Mastodon</a>
          (the free and federated alternative to Twitter) and <a href="https://joinpeertube.org">PeerTube</a>
          (the free and federated alternative to YouTube) instances.
        - What is certain is that the Mobilizon software will be distributed free
          of charge and that no one will have to pay us anything to install it on their
          servers (and we will even try to make your lives easier!).
        - Our hope is that the enthusiasm around this service gives us the means to
          open our own instance, public and free of use, perhaps when V1 is released…
    timeline-q:
      q: When can I use Mobilizon?
      a:
        p:
        - As of today, the code is still under development, and cannot be simply and
          conveniently installed and used.
        - 'In the fall of 2019, we plan to release a beta version, whose features
          will depend on the amount collected during crowdfunding. This version,
          which is equivalent to a first draft, will allow unstable use: the documentation
          will be in the process of being published, and there will likely still be
          improvements to be made.'
        - Note that if we reach the 2<sup>nd</sup>level, we will open a demonstration
          instance to the public, where everyone can click around and experiment,
          but where data will be deleted on a regular basis.
        - It is only in the first half of 2020, when version 1 is released, that Mobilizon
          will be easily usable by everyone.
  crowdfunding:
    title: Crowdfunding questions
    third-platform:
      q: Why not use a platform like GoFundMe or Kickstarter?
      a:
        p:
        - Framasoft is an <i lang="fr">association</i> domiciled in France, which
          means we can’t use many platforms that require a tax domicile in North
          America. In addition, the French platforms (which <a href="https://fr.ulule.com/etherpad-framapad/">we
          have already used</a>, on <a href="https://www.kisskissbankbank.com/fr/projects/peertube-a-free-and-federated-video-platform">multiple
          occasions</a>) do not offer a tool that exactly complies with the crowdfunding
          model we wanted to use this time.
        - So we decided to save on the 5% commission that these intermediaries earn
          on average, and to create our own fundraising website. We already had the
          banking and accounting tools to collect donations, so we just needed to create
          the web interface.
        - The advantage of having a homemade tool is that your data does not pass
          through yet another intermediary. As with all the <a href="https://degooglisons-internet.org">ethical
          services</a> we have been offering for years, <a href="https://framasoft.org/fr/cgu/">we’re not
          after your data</a>, because we sincerely value your privacy.
    trust:
      q: How do I know if I can trust you?
      a:
        p:
        - This is a question that must be asked, because everyone has their own levels
          of trust, which depend on several factors. We can tell you a bit about us,
          but it will be up to you to make the call.
        - 'Framasoft is a not-for-profit association (under the French 1901 Act):
          our aim is not to make money, just to earn the means to pursue our actions
          and achieve financial balance. So we have no interest in exploiting your
          data, profiling you, etc.'
        - 'Framasoft’s business model is generosity: we are funded through individuals’
          donations and have been for years. Our accounts are audited by an independent
          auditor, whose <a href="https://framasoft.org/fr/association/">reports
          are published here (in French)</a> (the 2018 report is currently being processed).'
        - Our actions are known and recognised throughout the French-speaking free software
          community. From our <a href="https://framalibre.org ">free resources directory</a>
          to our <a href="https://framabook.org ">free licenced book publishing house</a>,
          to the <a href="https://degooglison-internet.org ">34 alternative services</a>
          that we host to Degooglise the Internet, time and again we have demonstrated our
          commitment to strong ethics.
        - After more than 15 years of existence, Framasoft’s reputation is well established
          (look us up!). We have already had crowdfundings to finance the development
          of <a href="https://fr.ulule.com/etherpad-framapad/">MyPads</a> and <a href="https://www.kisskissbankbank.com/fr/projects/peertube-a-free-and-federated-video-platform">PeerTube</a>,
          which we have delivered <a href="https://joinpeertube.org/en/news/">beyond
          what was promised</a> (and funded) during those campaigns.
    noreward:
      q: Why don't you offer any rewards?
      a:
        p1:
        - Because the true goal for all of us is the creation of a digital <em>common</em>.
          Indeed, Mobilizon is distributed <a href="https://framagit.org/framasoft/mobilizon/blob/master/LICENSE">under
          a free licence</a>, which means that it belongs to everyone, without anyone
          being able to appropriate it exclusively.
        - The reward or perk system of traditional crowdfunding costs a lot of time, money
          and energy to project leaders. But we are betting that enough people will be willing
          to fund a credible alternative to Facebook events, just for the sake of making it
          available to everyone.
        - Lastly, we don’t want a reward system that discriminates between contributors
          based on their income, because everyone has different means, and everyone
          can participate, according to their means, in the birth of a digital common.
        - 'Thus, any person who contributes to financing Mobilizon will be entitled
          (if they so wish) to:'
        ul:
        - get their (nick)name added to Mobilizon’s readme file and Joinmobilizon.org’s
          Hall of Fame;
        - receive the Mobilizon newsletter.
    receipt:
      q: Will I get a receipt for my donation?
      a:
        p:
        - Yes, provided that you tick the appropriate box on the donation form and
          provide us with a mailing address, which is required by law for us to issue
          a donation receipt.
        - Your receipt will be automatically generated in March 2020, and sent to
          the email address you have provided.
        - If you need a receipt before this date, or if you change your email address,
          you can <a href="https://contact.framasoft.org">contact us here</a>.
    taxes:
      q: Will my donation be tax deductible?
      a:
        p:
        - Only for French taxpayers.
        - Framasoft being an “association of public interest” (under French classification),
          any donation can entitle you to <a href="https://framablog.org/2018/11/22/impots-et-dons-a-framasoft-le-prelevement-a-la-source-en-2019/">a
          66% reduction (FR)</a> on your income taxes.
        - Thus, a “UX interview” donation of €130 would, after deduction, cost French
          taxpayers €44.20.
        - If you do not pay income taxes in France, please check with your country’s
          tax authorities.
news:
  title: What’s new with Mobilizon?
  subtitle: Discover the latest improvements of the tool
  last-post: Latest posts
  blocs:
    19-05-16:
      title: You’ve already made €15,000 worth of donations! Only €5,000 more to reach the first milestone!
      text:
        p0:
          - 'Thank you! 2 days after launching our fundraising campaign, we have almost reached the 1<sup>st</sup> milestone!
            We just achieved €15,000 and hope to reach the 1<sup>st</sup> milestone of €20,000 during the weekend.
            Such a crazy surge of donations gives us a lot of hope: not only hope of reaching the €20,000 milestone,
            but also hope of reaching our overall goal!'
          - 'And now, it’s up to you!'
          - 'We are a small French non-governmental organisation that knows how to reach out to the French FLOSS community.
            But it is more difficult for us to communicate about Mobilizon to a larger audience.
            We need you to help us reach alternative communities (activists, freedom fighters, environmental campaigners, etc.).
            So please go ahead and:'
        ul:
          - 'Share the information on your favourite social networks (even those we don’t really like!) using <em>#JoinMobilizon</em>!'
          - Share the <a href="https://joinmobilizon.org/en/">joinmobilizon.org</a> website with
            your activist friends who also want to change the world!
          - Forward this information to the communities you are a part of. You can find inspiration and more info in
            <a href="https://framablog.org/2019/05/14/mobilizon-lets-finance-a-software-to-free-our-events-from-facebook/">our blog post</a>!
          - Send the info to every person who dreams of a more ethical and free tool to gather, organise and mobilise.
        p1:
          - 'Our only form of advertisement is the best we know: you!'
        p2:
          - <img src="@:data.baseurl/img/software_to_the_people_830x830_-_Mobilizon.png" class="img-fluid" alt="" />

    19-05-14:
      title: The levels of Mobilizon crowdfunding
      text:
        p0:
        - We believe in the need to create user-friendly software that allows people
          to organise their events away from Facebook or Meetup (and others).
        - We believe in it so deeply that we have invested time, work and money to imagine
          its design.
        - Since our association is financed only by donations, we set up this crowdfunding
          to ask you how far you want us to take the development of Mobilizon.
        h41: Level 1 - Free & basic version
        p1:
        - <img src="@:data.baseurl/img/en/01-palier-mobilizon-minimaliste.png" class="img-fluid"
          alt="">
        - This amount will cover our prototype expenses.
        - We will thus recover the sums fronted to design and promote the software,
          pay for the development time of its functionality as well as the work
          of the designers who contributed to its design.
        - The code will be returned to the community in a state that can be taken
          back by anyone.
        h42: Level 2 - Federated version
        p2:
        - <img src="@:data.baseurl/img/en/02-palier-mobilizon-federe.png" class="img-fluid"
          alt="">
        - 'We will be able to implement an essential protocol to make Mobilizon a
          success: the ActivityPub federation. Thanks to the federation, any community
          will be able to install Mobilizon on its own servers, and connect each
          instance to the rest of the network.'
        - This makes it possible to multiply the number of entry points and decentralise
          the data, and to connect to the fediverse which already includes Mastodon
          (an alternative to Twitter) and PeerTube (an alternative to YouTube).
        - We will also open a demo instance of this beta version, so that everyone
          can go and see for themselves what the software will look like.
        h43: Level 3 - Ideal version
        p3:
        - <img src="@:data.baseurl/img/en/03-palier-mobilizon-ideal.png" class="img-fluid"
          alt="">
        - This will give us the means to make the Mobilizon of our dreams come true!
        - Thus, Mobilizon will allow the creation of groups, with messaging tools
          and exchange between members. Mobilizon will also make it possible to display,
          organise and manage the external tools that your group already uses to
          create documents together or set the next meeting date, for example.
        - Lastly, each account holder will be able to create several identities,
          in order, for example, to display a different identity for sporting events,
          family reunions and activism marches.
        h44: And beyond – to version 1
        p4:
        - <img src="@:data.baseurl/img/en/04-palier-mobilizon-version-1.png" class="img-fluid"
          alt="">
        - 'We did the math: with €50,000, we will have the means to
          make the best software we can. If we receive more money, we won’t do more,
          or faster, but we can go further.'
        - In the fall of 2019, we will release a beta version. From then on, with
          the feedback and comments we will receive, we will work toward a first
          complete release, a V1, which we aim to be ready in the first half of 2020.
        - But there’s a lot of work to do until we get there. We dream of setting up
          a mapping server to facilitate the localisation of events, of creating a
          Mobilizon app for smartphones, and much more…
media:
  title: Press